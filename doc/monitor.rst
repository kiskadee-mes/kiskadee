Monitor - Module to manage kiskadee analysis
============================================

The Monitor class is responsible for dequeuing packages from the
`packages_queue` and for checking which packages need to be analyzed.

The class defines the following public behaviors:


.. autoclass:: kiskadee.monitor.Monitor()
    :members: monitor

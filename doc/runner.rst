Runner - Module to run static analysis
============================================

This module is responsible for running several static analyzers on
on a given project source code.

The module defines the following public behaviors:


.. automodule:: kiskadee.runner
   :members:


